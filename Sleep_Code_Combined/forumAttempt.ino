void forumAttempt(){
//All the commented out below was attempts taken from Arduino forum
//No measureable difference made on the power consumption

      //----------post #6 Re: Reducing Power Consumption-----------------
      //----------https://forum.arduino.cc/index.php?topic=428764.15-----
      //----------1.81 mA to 1.80----------------------------------------
      
        SERCOM0->USART.CTRLA.bit.ENABLE=0;
        SERCOM1->USART.CTRLA.bit.ENABLE=0;
        SERCOM2->USART.CTRLA.bit.ENABLE=0;
        SERCOM3->USART.CTRLA.bit.ENABLE=0;
        SERCOM4->USART.CTRLA.bit.ENABLE=0;
        SERCOM5->USART.CTRLA.bit.ENABLE=0;
       
        I2S->CTRLA.bit.ENABLE=0;
       
        ADC->CTRLA.bit.ENABLE=0;
       
        DAC->CTRLA.bit.ENABLE=0;
       
        AC->CTRLA.bit.ENABLE=0;
       
        TCC0->CTRLA.bit.ENABLE=0;
        TCC1->CTRLA.bit.ENABLE=0;
        TCC2->CTRLA.bit.ENABLE=0;
       
        RTC->MODE0.CTRL.bit.ENABLE=0;
        RTC->MODE1.CTRL.bit.ENABLE=0;
        RTC->MODE2.CTRL.bit.ENABLE=0;
       
        USB->HOST.CTRLA.bit.ENABLE=0;
        USB->DEVICE.CTRLA.bit.ENABLE=0;
        
      
      //----------post #7 Re: Reducing Power Consumption-----------------
      //----------https://forum.arduino.cc/index.php?topic=428764.15-----
      //----------1.80 mA to 1.80----------------------------------------
        
        //PM->APBBMASK.reg &= ~PM_APBBMASK_PORT;
      PM->APBBMASK.reg &= ~PM_APBBMASK_DMAC;
      PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM0;
      PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM1;
      PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM2;
      //PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM3;
      //PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM4;
      //PM->APBCMASK.reg &= ~PM_APBCMASK_SERCOM5;
      PM->AHBMASK.reg &= ~PM_AHBMASK_USB;
      PM->APBAMASK.reg &= ~PM_APBAMASK_WDT;
      
      
      
      //----------post #18 Re: Reducing Power Consumption----------------
      //----------https://forum.arduino.cc/index.php?topic=428764.15-----
      //----------1.80 mA to 1.80----------------------------------------  
      GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_USB | GCLK_CLKCTRL_GEN_GCLK4;
      GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_EVSYS_0 | GCLK_CLKCTRL_GEN_GCLK4;
      //uint8_t clockId = 0;
      //  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID( clockId ) | // Generic Clock 0 (SERCOMx)
      //                      GCLK_CLKCTRL_GEN_GCLK0 | // Generic Clock Generator 0 is source
      //                      GCLK_CLKCTRL_CLKEN ;
      
      
      
      //----------random attempt to remove floating pins-----------------
      //-----https://github.com/adafruit/Adafruit_SleepyDog/issues/17----
      //-----https://github.com/arduino/ArduinoCore-samd/blob/master/cores/arduino/wiring.c#L85
      //----------1.80 mA to 1.80----------------------------------------
      
        
        // Setup all pins (digital and analog) in INPUT mode (default is nothing)
        for (uint32_t ul = 0 ; ul < NUM_DIGITAL_PINS ; ul++ )
        {
          pinMode( ul, OUTPUT ) ;
          digitalWrite(ul,LOW);
        }
        delay(1000);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(1000);
        digitalWrite(LED_BUILTIN, LOW);
      
      //----------random attempt to remove floating pins-----------------
      //-----https://www.avrfreaks.net/forum/samd21-standby-power-consumption----
      //----------1.80 to 1.81 mA ----------------------------------------
      
      USBDevice.detach();
      
      SERCOM0->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM0->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      SERCOM1->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM1->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      SERCOM2->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM2->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      SERCOM3->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM3->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      SERCOM4->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM4->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      SERCOM5->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_RUNSTDBY;
      SERCOM5->USART.CTRLA.reg &= ~SERCOM_USART_CTRLA_ENABLE;
      
      ADC->CTRLA.reg &= ~ADC_CTRLA_RUNSTDBY;
      ADC->CTRLA.reg &= ~ADC_CTRLA_ENABLE;
      

}
      
