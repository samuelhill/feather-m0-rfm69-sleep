//Feather M0 RFM69 
//Adafruit SAMD version 1.8.6
//Arduino SAMD version 1.5.11

/*
  TimedWakeup

  This sketch demonstrates the usage of Internal Interrupts to wakeup a chip in sleep mode.
  Sleep modes allow a significant drop in the power usage of a board while it does nothing waiting for an event to happen. Battery powered application can take advantage of these modes to enhance battery life significantly.

  In this sketch, the internal RTC will wake up the processor every 2 seconds.
  Please note that, if the processor is sleeping, a new sketch can't be uploaded. To overcome this, manually reset the board (usually with a single or double tap to the RESET button)

  This example code is in the public domain.
*/

#include <ArduinoLowPower.h>
#include <ZeroRegs.h>


#define dumpRegisters
#define radio

#ifdef radio
//radio Libraries
  //RadioHead library
  #include <RH_RF69.h>
  #include <RHReliableDatagram.h>
  
  //LowPowerLab RFM69 Library
  #include <RFM69.h>
  #include <RFM69registers.h>
  #include <RFM69_ATC.h>
  #include <RFM69_OTA.h>

  #define RFM69_CS          8                          //Radio module pins
  #define RFM69_INT         3
  #define RFM69_RST         4
  //Radio Object Setup
    RH_RF69 rf69(RFM69_CS, RFM69_INT); 
#endif 

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  
  // Uncomment this function if you wish to attach function dummy when RTC wakes up the chip
  // LowPower.attachInterruptWakeup(RTC_ALARM_WAKEUP, dummy, CHANGE);

  //forumAttempt();

  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);


//----------dump registers------------------------------------------
//----------https://github.com/drewfish/arduino-ZeroRegs------------
//----------                ----------------------------------------

#ifdef dumpRegisters
    Serial.begin(9600);
    while (! Serial) {}  // wait for serial monitor to attach
    ZeroRegOptions opts = { Serial, false };
    printZeroRegs(opts);
#endif
  
}
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);

  #ifdef radio
    redditAttempt();
  #endif

 
  // Triggers a 2000 ms sleep (the device will be woken up only by the registered wakeup sources and by internal RTC)
  // The power consumption of the chip will drop consistently
  LowPower.deepSleep(5000);
  digitalWrite(LED_BUILTIN, HIGH);
}

void dummy() {
  // This function will be called once on device wakeup
  // You can do some little operations here (like changing variables which will be used in the loop)
  // Remember to avoid calling delay() and long running functions since this functions executes in interrupt context
}
